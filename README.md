# Docker (Swarm) Garbage Collection (Swarm 1.0.0) #

### What is this repository for? ###

This repository contains two scripts. The first is for removing docker-container, the second to remove old images.

#### Image-script ####

The image-script removes images based on excludes and time-constraints in Docker Swarm 1.0.0 (API 1.21). The script is intended to run as a cronjob. We run it at an interval of 30 minutes.

This script will use an image-database as source for the removal of an image. The database is realized as csv-file, which contains of 3 columns **(ImageId ,FirstSeen ,LastUsed)**.

You also can add images, that never should be deleted by adding the name of the image to the file "exclude" within the data directory. The name must be findable via the "docker images" command. This repository contains an example file within the "data"-directory.

The script scans the images available via the "docker images" command and will check for the new arrival of images. New images are added to the image-database with their id, the current timestamp and an empty entry in last_used. Furthermore the exclude-file is read and the corresponding image ids are computed and added to the exclude list. 

Afterwards the script removes dangling (<none>:<none>) images on all nodes in the swarm.

As next step the running containers are scanned and their last_used timestamps in the database are updated. The running containers are also put on the exclude list.

Besides the script will take care of double named images, i.e. there are 2 images, both named ubuntu:14.04, but under id a and b. It will detect such cases and create tuples of (imageid,imageage) and delete all except the youngest one.

As last step the script will delete all "dead" (not within excludes and unused for more than n days) images. If an image is double tagged, i.e. imageid "23x" is tagged as ubuntu:14.04 and ubuntu:latest, this image will be deleted by removing all corresponding tags, otherwise its just removed by id. Before removing, it tries to bring the removal-list in an order, where as less conflicts as possible happen. ( I.e. you can`t delete an image if they have a dependent child, which is in use.)  

### What do i need ? ###
* Docker Client 1.91 (API 1.21 / 1.24 )
* Docker Swarm 1.0 (API 1.21 / 1.24) 
* Python 2.7.6 
* Permissions to write to /run/lock/.. (for pid-file => 2 instances can´t run parallel).
    * If you don`t have those permissions you can modify the value of LOCK_PATH to another path, where you are allowed to write.

### How do i run the script ###

* Clone this repository.
* cd dockerimagecollection.
* Add excludes of your needs to data/exclude (if you want to use the script from this directory). Or create your own folder, add a file called "exclude" and pass it via -d "path to your directory".
* ./bin/docker_image_collection.py -H "DOCKER_HOST" -d "yourdatafolder" [-t "safetimeindays" | -tu "safetimeununesd" -ts "safetimeunseen"] [-l "logfolder"] [-n] [-s] [--debug]
    * ** NEEDED PARAMETER **  
    * -H "DOCKER_HOST" Socket to communicate with docker. Possible protocols are tcp:// unix:// or fd://. You need to specify one of those. If nothing is specified while installing docker and it is running on your machine, -H unix:///var/run/docker.sock can be used. If you specified to communicate with docker via tcp://, i.e. if it runs on a remote machine you can access it via tcp-socket ,i.e. swarm-master:2377 or ip:port. 
    * -d "datafolder" Within this folder the imagedb will be created and updated. Here you also can specify the exclude-file.
    * ** Optional Parameter **
    * -n DRY-RUN option. If you run the script with this flag nothing is going to be deleted. You just get a report of what would have been deleted.
    * -s Runs the script silent. So there is no output to stdout. Combined with -l your script will create silent create logfiles.
    * -l "logpath" By passing the -l flag the script will create logfiles within the specified folder. It will also remove logs older than 30 days. Only removes are logged.
    * --debug Enables excessive logging.
    * -t With this flag the gracetime in days for images can be set. Default is 7 days. Can be disabled by running -t "0".
    * --time-unseen Sets the time, that an image can survive after being seen for the first time.
    * --time-unused Sets the time, that an image can survive after being out of use of an container.

The bin/-folder also contains an example-script, which is run by our cron-daemon. In order to use it, the variables workdir and docker-url need to be fitted to your needs.

#### Container-script ####

This script will remove container. We also run this script by the cron-daemon in intervals of 30 minutes. It removes all container without set ressources, like -c and -m. The script also removes container, that exited 7 days ago. You also can whitelist container by either their container-id or the --no-trunc id of the image, which is run by the container.

In order to use it you have to configure 2 variables in the beginning of the script. First you have to configure the log-path, if you don`t want your logs within /var/tmp/. Second you need to adapt the value of docker_host to the socket of your docker-daemon. If you did not specify anything while installing docker, it should be unix:///var/run/docker.sock. Other possible protocols are fd:// and tcp://.