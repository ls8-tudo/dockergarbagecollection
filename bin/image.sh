#!/bin/bash

_e=0

# TODO : add your docker socket here.
dockerurl="unix:///var/run/docker.sock"

# TODO: add your working directory of the script here
workdir=""

datadir="./data"
logdir="./log"
fn_imagedb="./data/imagedb.csv"
pyprog="./bin/docker_image_collection.py"

PYTHON="/usr/bin/python"

[ -d "${workdir}" ] && {
  cd "${workdir}" &&
  { 
    [ -d "${datadir}" ] || mkdir "${datadir}" &&
    [ -d "${logdir}" ] || mkdir "${logdir}" &&
    [ -f "${fn_imagedb}" ] || {
      if [ -e "${fn_imagedb}" ]; then
        /bin/false
      else
        touch "${fn_imagedb}"
      fi
    } &&
    {
      #${pyprog} -H ${dockerurl} -d "${datadir}" -l "${logdir}" -s --debug -n
      #  
      #  -H  url (ex. "tcp://s876cnsm:2377" for swarmmaster) to communicate with docker
      #  -d  directory to store data between two runs
      #  -l  directory to store logs
      #  -s  silent no messages to stdout
      #  -t  grace period for not used images
      #  --container enable removing of unused and invalid containers
      #  --debug debug logging into log file 
      #  -n  dryrun
      ${PYTHON} "${pyprog}" -H "${dockerurl}" -d "${datadir}" -l "${logdir}" --debug -n -t 7 -s 
    }
  }
} > /dev/null 2>&1 
_e=${?}

exit ${_e}

