#!/usr/bin/python

# DockerImageCollection - Removing images based on excludes and timeconstraints.
#
# Copyright (C) 2016 Lukas Heppe, TU Dortmund Computer Science VIII
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

################ IMPORTS ########################

import argparse
import atexit
import csv
import datetime
import json
import logging
import os
import operator
import re
import shutil
import subprocess
import sys
import urllib2
import traceback

################ VARIABLES / PARAMETER################ 

# contains images/repos, that shouldnt be deleted 
EXCLUDE_PATH = "data/exclude" 
# image db (image_id,first_seen,last_used)
IMAGE_DB = "data/imagedb.csv" 
# logging file
LOGGING_FILE ="log/"
# log files will be deleted after x days
LOG_SAVE_DURATION = 30
# Days, that images will survive, after usage in container.
DAYS_UNUSED = 7
# Days, that images will survive, after download.
DAYS_UNSEEN = 7
# Nothing gets deleted if set to one
DRY_RUN = 0
# Logging to file. By default disabled. Can be activated via -l "logfolder" switch.
LOGGING = 0
# PATH FOR PID-FIlE
LOCK_PATH ="/run/lock/docker-gc/docker-gc.pid"
#DEBUG MODE
DEBUG = 0
#Docker Host Variable
DOCKER_HOST = "s876cnsm:2377"

################ UTILS CLI / LOGGING ################ 

# creates and argument parser
def get_parser():
	parser = argparse.ArgumentParser(description = "./docker_image_collection")
	parser.add_argument("-d","--data",help="Directory for data about images and excludes.")
	parser.add_argument("-l","--log",help="Directory for creating logs. By default disabled.")
	parser.add_argument("-t","--time",type=int,metavar='[0-*]', help= "Time in days, that image can survive, without being used after beeing seen for the first time. -t 5 <==> --time-unseen 5 --time-unused 5")
	parser.add_argument("-s","--silent",action="store_true", help="Runs this programm silently. No stdout.")
	parser.add_argument("-n","--dry-run", action="store_true", help="Runs this programm without deleting anything.")
	parser.add_argument("-H","--docker-host",help="Docker environment to communicate with docker. Exp. : tcp://s876cnsm:2377.")
	parser.add_argument("--time-unused",type=int,metavar='[0-*]', help="Time in days, that image can survive, without being used.")
	parser.add_argument("--time-unseen",type=int,metavar='[0-*]', help="Time in days, that image can survive, after beeing seen. Default is 7 days.")
	parser.add_argument("--debug",action="store_true",help="Excessive logging.")
	return parser

# creates a logger, which logs to a file	
def get_logger(args):
	logger = logging.getLogger("dockerlogger")	
	formatter = logging.Formatter('%(asctime)s - %(message)s')
	if LOGGING == 1 :		
		file_handler = logging.FileHandler(LOGGING_FILE,mode='w')
		file_handler.setFormatter(formatter)
		logger.addHandler(file_handler)
	elif LOGGING == 0 and args.silent:		
		logger.disabled = True		
	# if not silent log to stdout
	if not args.silent:
		# create stream handler for stdou	
		std_handler = logging.StreamHandler(sys.stdout)
		logger.addHandler(std_handler)
		std_handler.setFormatter(formatter)	
	# set log level dependening on cli option
	if args.debug :
		logger.setLevel(logging.DEBUG)
	else:
		logger.setLevel(logging.INFO)		
	return logger

# handles command line attributes
def handle_attributes(args,parser):
	logger = logging.getLogger("dockerlogger")
	global DRY_RUN 
	global EXCLUDE_PATH
	global LOGGING
	global LOGGING_FILE
	global DAYS_UNUSED
	global DAYS_UNSEEN
	global DEBUG
	if args.docker_host:
		global DOCKER_HOST
		DOCKER_HOST = args.docker_host
	else:
		sys.stderr.write("\nFATAL ERROR : Need DOCKER_HOST variable to communicate with docker.\n\n")
		parser.print_help()
		sys.exit(0)
	if args.data:			
		EXCLUDE_PATH = os.path.join(args.data,"exclude")
		IMAGE_DB = os.path.join(args.data,"imagedb.csv")
		create_file(IMAGE_DB)
	else :
		sys.stderr.write("\nFATAL ERROR : Need location to store informations about the images. Please provide a path to a folder via -d switch.\n\n")
		parser.print_help()
		sys.exit(0)
	if args.log:
		LOGGING = 1
		LOGGING_FILE = os.path.join(args.log,("docker-gc.log."+datetime.datetime.now().isoformat()))
		create_file(LOGGING_FILE)
	if args.time and (args.time_unused or args.time_unseen):
		sys.stderr.write("\nThe flag -t is not valid in combination with --time-unused and/or -time-unseen. Either specify it alone or adjust the timings with -time-unseen and/or --time-unused.\n\n")
		parser.print_help()
		sys.exit()
	elif args.time == 0:		
		DAYS_UNUSED = args.time
		DAYS_UNSEEN = args.time
		logger.debug("Disabled time filer.")
	elif args.time:
		if(args.time < 0):
			sys.stderr.write("Only positive values are allowed for time.\n")
			parser.print_help()
			sys.exit()
		DAYS_UNUSED = args.time
		DAYS_UNSEEN = args.time
		logger.debug("Images are safe for " + str(DAYS_UNUSED) + " days after use." ) 
		logger.debug("Images are safe for " + str(DAYS_UNSEEN) + " days after download." ) 
	if args.time_unused:
		if(args.time_unused < 0):
			sys.stderr.write( "Only positive values are allowed for time.")
			parser.print_help()
			sys.exit()
		DAYS_UNUSED = args.time_unused
		logger.debug("Images are safe for " + str(DAYS_UNUSED) + " days after use." )
	if args.time_unseen:
		if(args.time_unseen < 0):
			sys.stderr.write( "Only positive values are allowed for time.")
			parser.print_help()
			sys.exit()
		DAYS_UNSEEN = args.time_unseen		
		logger.debug("Images are safe for " + str(DAYS_UNSEEN) + " days after download." ) 
	if args.dry_run:
		logger.info("Starting dry-run.")
		DRY_RUN = 1
	if args.debug:
		DEBUG = 1

################ LOCK / UNLOCK ################ 

def lock():
	# check if lock-path is existing. if yes abort, else create lock
	if not os.path.exists(os.path.dirname(LOCK_PATH)):
		os.makedirs(os.path.dirname(LOCK_PATH))
		os.mknod(LOCK_PATH)
		with open(LOCK_PATH,'r+') as f:
			f.write(str(os.getpid())+"\n")
		atexit.register(unlock)
	else:
		sys.stderr.write("FATAL ERROR: An instance of this is already running.\n")
		sys.exit(1)
	
def unlock():
	if os.path.exists(os.path.dirname(LOCK_PATH)):
		rm_r(os.path.dirname(LOCK_PATH))

def rm_r(path):
    if not os.path.exists(path):
        return
    if os.path.isfile(path) or os.path.islink(path):
        os.unlink(path)
    else:
        shutil.rmtree(path)

################ AUTO REMOVE OF LOGS OLDER THAN 30 DAYS ################ 

# returns the relative path to logs, which are older than 30 days.
def find_old_logs(path):
	old_logs = list()
	path = os.path.dirname(path)
	logs = os.listdir(path)
	for log in logs:
		if ".log." in log:
			log_split = log.split(".log.")
			#log_split[1] contains the date as string
			now = datetime.datetime.now()
			log_date = datetime.datetime.strptime(log_split[1],"%Y-%m-%dT%H:%M:%S.%f")
			if days_between(now,log_date) >= LOG_SAVE_DURATION :
				old_logs.append(os.path.join(path,log))
	return old_logs

# removes logs from logging-path, which are older than 30 days
def remove_old_logs(path):
	logger = logging.getLogger("dockerlogger")
	logger.debug("Removing old logs.")
	for log in find_old_logs(path):
		logger.debug("Deleting old log " + log)
		os.remove(log)
	
################ HELPER FOR SCRIPT ################ 
		
def unused_x_days(image_id):
	now = datetime.datetime.now()
	delete_seen = False
	delete_used = False
	if not image_id in imagedb:
		return False	
	tpl = imagedb[image_id]
	# tpl[0] = firstseen tpl[1] = last_used
	if not tpl[0] == "-":
		time = datetime.datetime.strptime(tpl[0],"%Y-%m-%d %H:%M:%S.%f")
		diff = days_between(time,now)			
		if(diff >= DAYS_UNSEEN ):
			delete_seen = True
	else:
		delete_seen = True
	if not tpl[1] == "-":
		time = datetime.datetime.strptime(tpl[1],"%Y-%m-%d %H:%M:%S.%f")
		diff = days_between(time,now)			
		if(diff >= DAYS_UNUSED ):
			delete_used = True
	else:
		delete_used = True
	return delete_seen and delete_used

# returns differnce in days between 2 dates
def days_between(date1, date2):
	return abs((date2 - date1).days)

###### DOCKER FUNCTIONS ##############
	
# returns the names of all double named images
def get_double_named_images():
	p1 = subprocess.Popen(["docker","-H",DOCKER_HOST, "images"], stdout=subprocess.PIPE)
	p2 = subprocess.Popen(["awk","$2 != \"TAG\" {print $1\":\"$2}"], stdin=p1.stdout, stdout=subprocess.PIPE)
	p3 = subprocess.Popen(["sort"], stdin=p2.stdout, stdout=subprocess.PIPE)
	p4 = subprocess.Popen(["uniq","-d"], stdin=p3.stdout, stdout=subprocess.PIPE)
	liste = list()
	for item in p4.stdout:
		item = item.rstrip()
		liste.append(item)
	return liste

# returns all ids for an image_name
def get_ids_for_double_named(image_name):
	p1 = subprocess.Popen(["docker","-H",DOCKER_HOST, "images","--no-trunc",image_name], stdout=subprocess.PIPE)
	p2 = subprocess.Popen(["awk", "{print $3}"], stdin=p1.stdout, stdout=subprocess.PIPE)
	ret = list()
	for line in p2.stdout:
		line = line.rstrip()
		if( not line == 'IMAGE'):
			ret.append(line)
	return ret

# returns the id(s) to an image
def get_ids_for_image(image_name):
	p1 = subprocess.Popen(["docker","-H",DOCKER_HOST, "images","--no-trunc",image_name], stdout=subprocess.PIPE)
	p2 = subprocess.Popen(["grep","-v","IMAGE ID"], stdin=p1.stdout, stdout=subprocess.PIPE)
	p3 = subprocess.Popen(["awk", "{print $3}"], stdin=p2.stdout, stdout=subprocess.PIPE)
	images = list()
	for image_id in p3.stdout:
		image_id = image_id.rstrip()
		images.append(image_id)
	return images

# returns age of the image in days
def get_image_age(image_id):
	inspect = subprocess.check_output(["docker","-H",DOCKER_HOST, "inspect",image_id])
	inspect = json.loads(inspect)
	now = datetime.datetime.now()
	timeCreated = datetime.datetime.strptime(inspect[0]["Created"].split(".")[0], "%Y-%m-%dT%H:%M:%S")
	diff = days_between(now,timeCreated)
	return diff

#  returns true if the DOCKER_HOST is a swarm manager
def is_cluster():
	info = subprocess.check_output(["docker","-H",DOCKER_HOST,"version"]);
	return bool(re.search("swarm",info))

# returns the dangling images, if you are NOT in a swarm
def get_dangling():
	ret = list()
	p1 = subprocess.Popen(["docker","-H",DOCKER_HOST, "images","-q","-f","dangling=true"], stdout=subprocess.PIPE)
	for line in p1.stdout:
		line = line.rstrip()
		ret.append(line)
	return ret

# returns nodes in the cluster based on docker info in form ip:port
def get_nodes(): 
	nodes = list()
	p1 = subprocess.Popen(["docker","-H",DOCKER_HOST, "info"], stdout=subprocess.PIPE)
	p2 = subprocess.Popen(["grep","-Eo","[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\:[0-9]{2,5}"], stdin=p1.stdout, stdout=subprocess.PIPE)
	for line in p2.stdout:
		nodes.append(line.rstrip())
	return nodes
		
# returns all "dangling" images for a node. (needs ip:dockerport(2375) or nameofnode:dockerport)
def get_dangling_image_ids(node):
	images = list()
	p1 = subprocess.Popen(["docker","-H", node,"images","--no-trunc","-q","-f","dangling=true"], stdout=subprocess.PIPE)
	for line in p1.stdout:
		images.append(line.rstrip())
	return images	
	
# list all --no-trunc ids of the images in use by container
def get_container_images():
	images = list()
	# this are the container ids
	p1 = subprocess.Popen(["docker","-H",DOCKER_HOST,"ps","-q","-a"], stdout = subprocess.PIPE)
	for container_id in p1.stdout:
		container_id = container_id.rstrip()
		p2 = subprocess.check_output(["docker","-H",DOCKER_HOST,"inspect",container_id])
		a = json.loads(p2)
		images.append(a[0]["Image"])
	return images
	
############# CSV / FILE FUNCTIONS ############

def backup_file(src,des):
	shutil.copy(src,des)

# creates an empty file with its parents if the file isnt existing
def create_file(filename):
	if not os.path.exists(filename):
		if not os.path.exists(os.path.dirname(filename)):
			os.makedirs(os.path.dirname(filename))
		os.mknod(filename)
		
############ REMOVE IMAGE FUNCTIONS ################

# removes an image under consideration of dryrun
def rem_image(image_id,dry_run):
	arg_list = list()
	arg_list.append("docker")
	arg_list.append("-H")
	arg_list.append(DOCKER_HOST)
	arg_list.append("rmi")	
	arg_list.append(image_id)
	if dry_run == 1:
		logger.info("Dry-run: " + str(arg_list))
	else:
		logger.info(str(arg_list))
		subprocess.call(arg_list)
		# remove the image from db
		remove_image(image_id)
	
# untags the image, if its unused/downloaded for x or more days
def untag(image_id):
	if unused_x_days(image_id):
		rem_image(image_id,DRY_RUN)
	else:
		logger.info("Not untagging image " + image_id + ". Download/used less than " + str(DAYS_UNSEEN) + " ago.")	

###################### IMAGE DB #########################################

imagedb = {} # imagedb[i] = (first_seen,last_used)

# read the imagedb from disk as dict
def read_imagedb():
	with open(IMAGE_DB,"r+") as imagedbcsv:
		reader = csv.reader(imagedbcsv)
		for line in reader:
			if line:
				if len(line) == 3:
					imagedb[line[0]] = (line[1],line[2])

# safe the imagedb back to disk as csv
def safe_imagedb():
	with open(IMAGE_DB,"w+") as imagedbcsv:
		writer = csv.writer(imagedbcsv)
		for x in imagedb:
			writer.writerow([ x,imagedb[x][0], imagedb[x][1]])

# adds an image that is seen for the first time			
def add_image(image_id,first_seen):
	imagedb[image_id] = (first_seen,"-")
	
# removes an image from the list
def remove_image(image_id):
	if image_id in imagedb:
		del imagedb[image_id]

# updates the value first_seen of an image
def update_first_seen(image_id,new_date):
	if image_id in imagedb:
		tpl = imagedb[image_id]
		imagedb[image_id] = (new_date,tpl[1])

# updates the value last_used of an image
def update_last_used(image_id,new_date):
	if image_id in imagedb:
		tpl = imagedb[image_id]
		imagedb[image_id] = (tpl[0],new_date)			

# reads all images and puts them into a dict under id - (id, <repo:tag>[])
def read_images():
	dict = {} 
	p1 = subprocess.Popen(["docker","-H",DOCKER_HOST,"images","--no-trunc"],stdout = subprocess.PIPE)
	for line in p1.stdout:
		line = line.rstrip()	
		lines = re.split("\s+",line)
		# repo,tag,id
		repo = lines[0]
		tag = lines[1]
		id = lines[2]
		if not "IMAGE" in id and ( not repo in "<none>" or not tag in "<none>"):
			if id in dict:
				tpl = dict[id]
				tpl[1].append(repo+":"+tag)
			else:
				_list = list()
				_list.append(repo+":"+tag)
				dict[id] = (id,_list)
	return dict

# checks the list image_ids for the arrival of new images. if found one its appended to images_seen..	
def look_for_new_images(images):
	for key in images:
		if not key in imagedb:
			add_image(key,str(datetime.datetime.now()))

############### Search for parents of image #####################

# returns the parents to an image

def find_parents(image_id):
	try:
		p1 = subprocess.check_output(["docker","-H",DOCKER_HOST,"history","-q","--no-trunc",image_id]).split("\n")
		if "" in p1:
			p1.remove("")
		return p1
	except:
		return []
		
########### MAIN SCRIPT ##################

if __name__ == "__main__":
	try:		
		############ INIT ###############
	
		lock()				#lock with pidfile
		parser = get_parser()		#create cli parser
		args = parser.parse_args()	
		handle_attributes(args,parser)  #handle cli attributes
		logger = get_logger(args)	#setup logger
		atexit.register(safe_imagedb)   #safe image db on shutdown
		############ END INIT ###############

		logger.info("Starting docker-garbage-collection.")
	
		backup_file(IMAGE_DB,IMAGE_DB+".old")
	
		imagedb = {}
		
		# imagedb[id] = (first_seen,last_used)
		read_imagedb()
	
		# (id , list<repo:tag>)
		images = read_images()
	
		if LOGGING == 1:
			remove_old_logs(LOGGING_FILE)

		logger.debug("Found " + str(len(images)) + " images.")
	
		# compute exclude list
		# list of images, that are safe because of the exclude file
		file_excludes = list()

		if is_cluster() :
			logger.debug("Looking for dangling images on each node.") 

			# get dangling images (<none>:<none>)
			length = 0
			for node in get_nodes():
				danglimg = get_dangling_image_ids(node)
				if(len(danglimg) > 0):
					length = length + len(danglimg)
					logger.debug("Found " + str(len(danglimg)) + " images on node " + node + ".")
					logger.debug( node + " <-> " + str(danglimg))
					for image in danglimg:
						if DRY_RUN == 0:
							logger.info("docker -H " + node + " rmi " + image)
							subprocess.call(["docker","-H",node,"rmi",image])
						else:
							logger.info("Dry-run: docker -H " + node + " rmi " + image)	
			if(length > 0):
				logger.debug("Removed " + str(length) + " dangling (<none>:<none>) images." )	
		else :
			logger.debug("Looking for dangling images")
			dangling = get_dangling()
			if DRY_RUN == 0:
				logger.debug("[docker,rmi,`docker images -aqf dangling=true`]")
				for image in dangling:
					logger.info("[docker,rmi," + image + " ]")
					subprocess.call(["docker","-H",DOCKER_HOST,"rmi",image])
			else:
				for image in dangling:
					logger.info("Dry-run : [docker,rmi,"+image + " ]")
	
		# <none>:<none> images shouldnt be in image db	
		look_for_new_images(images)

		# exclude ids contains all id`s that shouldnt be removed
		if os.path.exists(EXCLUDE_PATH):
			logger.debug("Found exclude file at " + EXCLUDE_PATH + ".")
			logger.debug("Following images are excluded, because they are within excludefile.")
			with open(EXCLUDE_PATH,'r') as excludeFile:
				for line in excludeFile:			
					line = line.rstrip()
					if not line == "" :
						logger.debug("Excluding images that match " + line + ".")
						for exclude_image_id in get_ids_for_image(line):
							if exclude_image_id not in file_excludes:
								logger.debug("Excluding image with id : " + exclude_image_id)
								file_excludes.append(exclude_image_id)
		else :
			logger.info("No exclude file found.")

		logger.debug("Following images are excluded, because they used are within a container.")
	
		images_inuse = list()
		images_usedby_container = get_container_images()
		for image_id in images_usedby_container:
			if image_id not in images_inuse :
				if image_id is not None:
					logger.debug("Excluding image with id : " + image_id)
					images_inuse.append(image_id)
					# update of imagedb when i see an image
					if image_id in imagedb:
						update_last_used(image_id,str(datetime.datetime.now()))
					else:
						imagedb[image_id] = (str(datetime.datetime.now()),str(datetime.datetime.now()))
			
		# list of images with 1 name but stored under different ids
		double_named_images = get_double_named_images()

		logger.debug("Following images exists under same name with different ids : " + str(double_named_images))
		logger.debug("Tuple consists of (ImageId, ImageAge)")

		# creates a list of tuples for every double named id and deletes all except the youngest one
		for item in double_named_images:
			ids = get_ids_for_double_named(item)	
			tuple_list = list()
			for idx in ids:
				# ensure no duplicates
				contained = False
				for t in tuple_list:
					if t[0] == idx:
						contained = True
				if not contained:
					tupl = (idx,get_image_age(idx))
					tuple_list.append(tupl)
			tuple_list.sort(key=lambda tup: tup[1])
			logger.debug(item + " <-> " + str(tuple_list))
			min_days = sys.maxint
			min_id = None
			# find minimum and delete all older images
			for (a,b) in tuple_list:
				if(b < min_days):
					min_days = b
					min_id = a
			for (a,b) in tuple_list:
				if not a == min_id:
					untag(a)

		# list of excluded ids			 		
		exclude_ids = list()
		# images in use by container	
		for image in images_inuse:
			exclude_ids.append(image)
		# images from exception list
		for image in file_excludes:
			exclude_ids.append(image)
	
		removes = list()
	
		# get parents for all images
		parents = {}
		used_parents = {}
	
		for id in images:
			# check if id is dead
			if id not in exclude_ids and unused_x_days(id):
				# find dependencies within this list and resolve them
				removes.append(id)
	 			 	
	 	# get parents of all images currently in use
	 	for image in images_usedby_container:
	 		used_parents[image] = find_parents(image)
	 	
	 	# contains dead images, that has no running child
	 	remove_list = list()
	 		
	 	# for all these images i have to look if an images what i want to delete is within a list
	 	for image in removes:
	 		for key in used_parents:
	 			if image in used_parents[key]:
	 				if image in imagedb:
		 				tpl = imagedb[image]
		 				# update the images because it depends anyway on the deletion of image from container
		 				imagedb[image] = (tpl[0],str(datetime.datetime.now())) 
		 				logger.debug("Skipping image " + image + " because its an parent of currently used image.")
		 				continue
	 			else:
	 				if image not in remove_list:
	 					remove_list.append(image) 
	 	# find parents for an image
	 	for image in remove_list:
			parents[image] = find_parents(image)
	
		# sort the images, so there are hopefully no tries to delete a child after a maximum of 100 iterations
		maxItr = 100
		i = 0
		changed = True
		while changed and i < maxItr:
			changed = False
			for image in remove_list:
				for compare in remove_list:
					indexImage = remove_list.index(image)
					indexCmp = remove_list.index(compare)
					if image in parents[compare] and not compare == image and indexImage > indexCmp: 
						remove_list.remove(compare)
						indexInsert = remove_list.index(image)+1					
						remove_list.insert(indexInsert,compare)
						changed = True
						i = i + 1
						break;
							
		logger.debug(str(len(remove_list)) + " images are going to be deleted.")
		# this is sorted, so that there shouldnt be any more dependencies
		for image in remove_list:
			tag_list = images[image][1]
			# remove all tags to delete the image
			if len(tag_list) > 1:
				for repotag in tag_list:
					rem_image(repotag,DRY_RUN)
				if DRY_RUN == 0:
					# removes the image from the image db if it has more than one tag
					remove_image(image)
			# just delete the image
			else:
				rem_image(image,DRY_RUN)
		# safe updated image db	
		logger.info("Finished job.")
	except:
		traceback.print_exc()
		sys.stderr.write("FATAL ERROR. SHUTDOWN")
