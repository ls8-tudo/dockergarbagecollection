#!/usr/bin/python

# DockerImageCollection - Removing images based on excludes and timeconstraints.
#
# Copyright (C) 2016 Sebastian Gerard, Florian Priebe, Lukas Heppe TU Dortmund Computer Science VIII
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
import os
import json
import re
import datetime
import sys

#Docker Host Variable
dockerHost = "s876cnsm:2377"
# log file
LOG = "/var/tmp/docker-container-cleanup.log"
#whitelisted container-ids
whitelist = []
# whitelist container by their image long id (i.E. if there are multiple instances like monitoring tools, which run on all nodes)
imageIdWhiteList = []

time = datetime.datetime.now().time()
date = datetime.datetime.now().date()
logfile = LOG + date.strftime("%Y%m%d") + "-" + time.strftime("%H%M")

#logfile with datetime ending

#command has to be a list of strings, where the first is the command, the second and following are the whitespace seperated parameters
#e.g. if you want docker info, command has to be ["docker", "info"]
#if something bad happens there will be a RuntimeError
#returns the output of the given command
def processOutput(cmd,my_env):
	p = subprocess.Popen(cmd, env = my_env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout_data, stderr_data = p.communicate()
	if p.returncode != 0:
    		raise RuntimeError("%r failed, status code %s stdout %r stderr %r" %(cmd, p.returncode, stdout_data, stderr_data))
	return stdout_data

#command has to be a list of strings, where the first is the command, the second and following are the whitespace seperated parameters
#e.g. if you want docker info, command has to be ["docker", "info"]
#if something bad happens there will be a RuntimeError
#makes a call with the given command and environment
def processCall(cmd,my_env):
	p = subprocess.Popen(cmd, env = my_env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout_data, stderr_data = p.communicate()
	if p.returncode != 0:
    		raise RuntimeError("%r failed, status code %s stdout %r stderr %r" %(cmd, p.returncode, stdout_data, stderr_data))
	else: 
		return 0

#redirect stdout to log file
sys.stdout = open(logfile, 'aw')
sys.stderr = open(logfile, 'aw')
print datetime.datetime.today().isoformat(" ")+": Docker Garbage collection. Removing containers...\n"

#set the environment for all subcalls
env = os.environ.copy()
env["DOCKER_HOST"] = dockerHost

#check for verbose logging
verbose = False
if ((len(sys.argv) == 2) and (sys.argv[1]=="-v")):
	verbose = True

#get ids of all container
try:
	ids = processOutput(["docker","ps","-qa"],env).split("\n")
except RuntimeError as err:
	print err
#remove empty strings 
ids = filter(bool, ids)

#call docker inspect for all containers
inspects = list()
for id in ids:
	try:
		out = processOutput(["docker","inspect",id],env)
		inspects.append(out)
	except RuntimeError as err:
		print err
		break

#convert to json objects, zip with respective ids, because we need these to remove the containers
jsonInspects = list(map(lambda x : json.loads(x)[0], inspects))
idInspects = list(zip(ids, jsonInspects))

#get list of valid user names

#divide containers to be removed into two lists, to differentiate the reason they are removed
deleteRes = list()
deleteAge = list()
deleteError = list()	

for (id, inspect) in list(idInspects):
	
	ressourcesSet = False
	nameSet = False
	tooOld = False
	error = False

	#protect whitelisted containers, no longer needed
	if (id in whitelist):
		continue

	if (inspect["Image"] in imageIdWhiteList):
		continue

	#protect swarm and registry
	if (re.search("swarm",inspect["Config"]["Image"])!=None or re.search("registry",inspect["Config"]["Image"])!=None ):
		continue
	
	#protect containers that are less than 30 minutes old
	timeCreated = datetime.datetime.strptime(inspect["Created"].split(".")[0], "%Y-%m-%dT%H:%M:%S")
        minutesElapsed = abs(timeCreated - datetime.datetime.utcnow()).total_seconds()/60
	if (minutesElapsed < 30):
                continue

	#check whether both cpu and memory are set
	if ((inspect["HostConfig"]["CpuShares"] != 0) and (inspect["HostConfig"]["Memory"] != 0)):
		ressourcesSet = True

	#check whether an error has occured in this container
	if(not (inspect["State"]["Error"] == "")):
		error = True
	
	#check whether container finished more than 7 days ago
	if (inspect["State"]["Running"]==False and not error):
		try:
			timeFinished = datetime.datetime.strptime(inspect["State"]["FinishedAt"].split(".")[0], "%Y-%m-%dT%H:%M:%S")
		except ValueError:
			if inspect["State"]["FinishedAt"] == "0001-01-01T00:00:00Z":
				error = True
			else:
				continue
		if (not error and abs(timeFinished - datetime.datetime.utcnow()).total_seconds()/86400.0 > 7.0):
			tooOld = True

	#prints out the booleans for each pair to debug
	#print id + " " + inspect["Name"] + " RS? " + str(ressourcesSet) + " NS? " + str(nameSet) + " Old?" + str(tooOld)
			
	#if all three parameters are set, but the container has been dead for less than 7 days, don't delete the container
	if (ressourcesSet and not tooOld and not error):
		continue

	#delete the rest, but remember the reason they were removed
	if (not ressourcesSet):
		deleteRes.append((id,inspect))
	elif (tooOld):
		deleteAge.append((id,inspect))
	elif (error):
		deleteError.append((id,inspect))

#print results and remove containers
print datetime.datetime.today().isoformat(" ")+ ": Removed for missing -c and -m:"
for (id,inspect) in deleteRes:
	if (processCall(["docker", "rm", "-f", id],env)==0):
		if(verbose):
			print json.dumps(inspect)+"\n"
		else:
			
			print datetime.datetime.today().isoformat(" ")+ ": " + id+"\t"+inspect["Config"]["Image"]+"\t"+inspect["Name"]

print "\n" + datetime.datetime.today().isoformat(" ")+ ": Removed because it stopped more than 7 days ago"
for (id,inspect) in deleteAge:
        if (processCall(["docker", "rm", "-f", id],env)==0):
                if(verbose):
                        print json.dumps(inspect)+"\n"
                else:
                        print datetime.datetime.today().isoformat(" ")+ ": " + id+"\t"+inspect["Config"]["Image"]+"\t"+inspect["Name"]

print "\n" + datetime.datetime.today().isoformat(" ")+ ": Removed because it has occured an error"
for (id,inspect) in deleteError:
       	if (processCall(["docker", "rm", "-f", id],env)==0):
                if(verbose):
                        print json.dumps(inspect)+"\n"
                else:
                        print datetime.datetime.today().isoformat(" ")+ ": " + id+"\t"+inspect["Config"]["Image"]+"\t"+inspect["Name"]

print "\n"
